#!/bin/bash
ls ../docker-compose-$1.yml > /dev/null 2>&1
if test $? -ne 0;then
	echo "\n *** ERROR AL IDENTIFICAR EL AMBIENTE DE TRABAJO ***
	Incluya el identificador de entorno: local | desarrollo | qa 
	ejemplo: sh export_db.sh local\n"
	exit
else
	container=`grep container_name ../docker-compose-$1.yml|grep db|cut -d":" -f2|tr -d ' '`
	user=`grep MYSQL_USER ../docker-compose-$1.yml|grep -v root|cut -d":" -f2|tr -d ' '`
	pass=`grep MYSQL_PASSWORD ../docker-compose-$1.yml|cut -d":" -f2|tr -d ' '`
	db=`grep MYSQL_DATABASE ../docker-compose-$1.yml|cut -d":" -f2|tr -d ' '`
	docker exec -i $container ls > /dev/null 2>&1
	if test $? = 0;then
		echo "\n * EXPORTANDO DATOS..."
		docker exec $container /usr/bin/mysqldump -h127.0.0.1 -u $user --password=$pass $db --force > ../../DB/$1/$db.sql \
		&& echo "** EXPORTACION DE BASE DE DATOS REALIZADA! \n"
	else
		echo "\n EL CONTENEDOR $container NO ESTA CORRIENDO \n"
	fi
fi
